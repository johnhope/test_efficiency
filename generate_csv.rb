require 'csv'
require 'httparty'

class GitLabTestEfficiencyImprovementsQuery
  def execute
    query = standard_params.merge({ labels: 'rspec profiling', created_after: '2020-08-01' })
    HTTParty.get(base_url, query: query, headers: headers).parsed_response
  end

  def base_url
    'https://gitlab.com/api/v4/projects/278964/merge_requests'
  end

  def standard_params
    { state: :merged, scope: :all, per_page: 100 }
  end

  def headers
    { 'Accept' => 'application/json', 'Private-Token' => ENV['GITLAB_API_TOKEN'] }
  end
end

te_mrs = GitLabTestEfficiencyImprovementsQuery.new.execute

CSV.open("test_efficiency_mrs-#{Time.now.utc.strftime('%F')}.csv", "w") do |csv|
  csv << ['Merge Date', 'Title', 'Author', 'URL', 'Query count']
  te_mrs.each do |mr|
    queries_saved = mr['description'].scan(/Queries saved:\s*(\d+)/i).flatten.first.to_i
    merge_date = Time.parse(mr['merged_at']).strftime('%F')
    csv << [merge_date, mr['title'], mr['author']['username'], mr['web_url'], queries_saved]
  end
end
